# HTML Inline Help Label

This package adds a new property editor that allows you to show HTML in your content pages as properties.  Why is this cool?  Well, we use it to provide inline help to the content editors.  It is HTML so it can display images or links to more information to content editors.  It has also been used to provide a Help tab on a Document Type for specific help for that page type and what they need to know.

You can create several Data Types with this property editor each with their own HTML as prevalues.

In addition to the inline html as a property, you can specify more HTML to be displayed on a dialog flyout.  This could be used to give in depth info about the page.


## Umbraco Install

Download the package from Our Umbraco [https://our.umbraco.org/projects/backoffice-extensions/inline-html-help-label](Inline HTML Help Label) and install into your instance.

After install, create a new Data Type with the type of HTML Inline Help Label.  Then assign it to any Document Type to as a property to display the label on a content page.




## Setup for Development

### Install Dependencies

```bash
cd src
npm install -g grunt-cli
npm install
```

### Build

```bash
grunt
```

### Watch

```bash
grunt watch
```

### Package

```bash
grunt package
```
