
## Setup for Development

### Install Dependencies

```bash
cd src
npm install -g grunt-cli
npm install
```

### Build

```bash
grunt
```

### Watch

```bash
grunt watch
```

### Package

```bash
grunt package
```
